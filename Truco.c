#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <locale.h>
#include "LIB/header.h"
//Linha de Raciocinio//
/*recebe os numeros de 0 a 23 (q a 3)
O jogador 1 entrega as cartas.
O Jogador dá a carta vira.
O Jogador 2 joga    // se as cartas forem boas então truca.
Decide a carta que ganha
Pontua a mão
*/
int main()
{
	init();
	carta();
	manilhar();
	setlocale(LC_ALL,"");
	do
	{
		system("clear||cls");
		maoatual();
		play();
		globalpont();
	}
	while(jogo.maoatual!=0);
	logger(10);
}